Setup instructions

npx create-react-app dist
cd dist
npm start

The app will be running on localhost:3000

npm install react-router-dom

How did you decide on the technical and architectural choices used as part of your solution?
As I was a PHP developer 6 years ago, I used to work with traditional web server and DB (eg: Apache + PHP + MYSQL)
In this challenge, this is my first time to use React JS to build a web page.
I've imported "react-router-dom" and "react-router" to keeps UI in sync with the URL

Are there any improvements you could make to your submission?
I think it has a better way to create an object and put those contents in and sort the result. As now, I just put contents in
multi-dimensional array and performs a sort.

What would you do differently if you were allocated more time?
better UI, css
Actually I have taken more time on this challenge as I am new in React and more than 6 years haven't touched any coding
