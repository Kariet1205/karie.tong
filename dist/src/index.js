import React from 'react';
import ReactDOM from 'react-dom';
//import { uiseHistory } from 'react-router-dom';
import { Link, BrowserRouter as Router,Switch, Route} from 'react-router-dom';
import Content from './content';
import './index.css';
import { useParams } from "react-router";

class ContentType extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      type: null
    };

  }

  reloadPage = () => {
    let { itm } = useParams();

    this.setState({
      type: itm
    });

    return <Content type={itm}/>;
 }

  componentDidUpdate(){
    window.onpopstate = e => {
       window.location.reload();
    }
  }

  contentType = (a) => {

    this.setState({
      type: a
    });
  }

  render(){

            const {type} = this.state;

              if (type)
              {
                  return <Content type={type}/>;
              }else {
                return (
                  <section className="App" >
                  <Router>

                  <div class ="item">
                  <Link to="/series" onClick={() => this.contentType("series")}><img src="../assets/series.png" width="200px" alt="Series" /></Link>
                  <span class="caption">Popular Series</span>
                  </div>
                  <div class ="item">
                  <Link to="/movie" onClick={() => this.contentType("movie")}><img src="../assets/movies.png" width="200px" alt="Movie" /></Link>
                  <span class="caption">Popular Movies</span>
                  </div>
                  <Switch>
                  <Route path="/:itm" component={() => (this.reloadPage())} />

                  </Switch>
                  </Router>
                  </section>
                  );
              }

      }
  }

ReactDOM.render(<ContentType />, document.getElementById('root'));
