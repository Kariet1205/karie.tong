import React from 'react';
//import ReactDOM from 'react-dom';
//import { uiseHistory } from 'react-router-dom';
//mport { Link, BrowserRouter as Router, Route } from 'react-router-dom';
//import { View, Image, StyleSheet } from 'react-native';
//Simport { Component } from 'react';
import './index.css'

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: null,
      items: []
    };
  }

componentDidMount() {
    fetch("https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json")
     .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.entries,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: false,
            error
          });
        }
      )
  }

  showPosterImage = (type) => {

    const { items} = this.state;
    let a = 1;
    let contents = [];
    //array_1[0] refers to title
    //array_1[1] refers to programType (series/movie)
    //array_1[2] refers to releaseYear
    //array_1[3] refers to Poster url
    let array_1 = [];

    //push json items into an array, then sort it
    items.map (itm => {
      let array_2 = [];
            array_2.push(itm.title);
            array_2.push(itm.programType);
            array_2.push(itm.releaseYear);
            array_2.push(itm.images['Poster Art']['url']);
            array_1.push(array_2);

    });

      array_1.sort();

      for(let i = 0 ; i < array_1.length; i++) {

        let val = array_1[i].toString();
        let res = val.split(",");

        if (res[1] === type && res[2]>= 2010 && a <= 21)
        {
          contents.push(  <div class ="images"><img src = { res[3] } width="150px" height="200px"  alt= { res[0] }/><span class="caption">{ res[0] }</span>
          </div>);
          a++;
        }
      }

       return contents;
  }

  render(){

	const { error, isLoaded} = this.state;
  const {type} = this.props;

		if (error) {
      			//return <div>Error: {error.message}</div>;
            return <div>Oops. something went wrong....</div>;
    		} else if (!isLoaded) {
      			return <div>Loading...</div>;
    		}
        else {

              if (type)
              {

               return (
                      <ul>
                      {this.showPosterImage(type)}
                      </ul>
                );
              }
      }
  }
}

export default Content;
